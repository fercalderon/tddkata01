class Calculadora:
    def calcular(self,cadena):
        respuesta=[]
        if cadena == "":
            respuesta=[0,0,0,0]
            return respuesta
        elif "," in cadena:
            numeros = [int(n) for n in cadena.split(',')]
            elementos = len(numeros)
            promedio = sum(numeros) / elementos
            respuesta=[elementos,min(numeros),max(numeros),promedio]
            return respuesta
        else:
            respuesta=[1,int(cadena),int(cadena),int(cadena)]
            return respuesta


