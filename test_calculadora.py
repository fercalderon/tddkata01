from unittest import TestCase
from Calculadora import Calculadora

class TestCalculadora(TestCase):
    def test_elementosMinimoMaximoPromedio(self):
        self.assertEqual(Calculadora().calcular(""),[0,0,0,0],"Cero Elemetos")

    def test_unElementoMinimoMaximoPromedio(self):
        self.assertEqual(Calculadora().calcular("1"), [1, 1, 1,1], "Un Elemento")
        self.assertEqual(Calculadora().calcular("5"), [1, 5, 5,5], "Un Elemento")

    def test_dosElementosMinimoMaximoPromedio(self):
        self.assertEqual(Calculadora().calcular("2,6"), [2, 2, 6, 4], "Dos Elementos")
        self.assertEqual(Calculadora().calcular("4,2"), [2, 2, 4,3], "Dos Elementos")

    def test_multiplesElementosMinimoMaximoPromedio(self):
        self.assertEqual(Calculadora().calcular("1,2,3"), [3, 1, 3,2], "Tres Elementos")
        self.assertEqual(Calculadora().calcular("11,12,5,20"), [4, 5, 20,12], "Cuatro Elementos")